# Mapa 1
```plantuml
@startmindmap
*[#Pink] Von-Neumann vs Hardvard
	*[#Orange] John von neumann
		*[#GREEN] Historia 	
			*[#YELLOW] Fisico-matematico nacio el 28 de diciembre de 1905 en Budapest Hungria \n*Su padre era banquero \n Pionero en la ciencia de la computacion  tuvo importantes contribuciones \n en el desarrollo de diseño logico y la programacion.
				*[#lightgreen] A su corta edad mostraba signos de ser un genio \n*Obtuvo un titulo en ingenieria quimica en 1925 \n*A los 23 años obtuvo su doctorado en matematicas
					*[#lightyellow] En 1933 cuando Hitler llego al poder renuncio a sus puestos academicos \n y se dedico a realizar estudios de teoria cuantica en  la universidad \n de Princeton y en el instituto de estudios avanzados 	
						*[#lightblue] En 1956 antes de morir obtuvo el premio Enrico Fermi \n Murio el 8 de febrero de 1857 en Washinton DC
		*[#GREEN] Aportaciones 
			*[#YELLOW] *Propuso el bit como medida en la memoria de ordenadores 0 y 1 \n*Resolvio el problema de la obtencion de respuestas fiables \n con componentes no fiables bit de paridad \n*Participo en el que se considera el primer ordenador ENIAC \n*Desarrollo programa que consistia en el almacenado \n en memoria de los datos \n*Propuso separar hardware de software \n*Contribuyo técnicas de implosión para la bomba atómica
	*[#Orange] Creo arquitectura de computadoras actuales
		*[#GREEN] CPU
			*[#YELLOW] Como tal es el que se encarga directamente de ejecutar las aplicaciones que recibe por parte del \n sistema operativo, el sistema operativo es el que le da las instrucciones al procesador para que \n sea este quien opere todas esas instrucciones.
				*[#lightgreen] Unidad aritmético lógica \n se encarga específicamente realizar las operaciones que tienen que ver con \n el procesamiento de los datos todos los datos dentro de un computador\n reciben instrucciones muy básicas de suma y resta. \n Qué son operaciones aritméticas y operaciones lógicas en las que ellos deciden \n  o el procesador decida hacerlas o no más por eso de ahí el nombre unidad aritmética\n lógica porque realiza operaciones que son aritméticas de suma y resta \n lógicas como operaciones de OR o NOT cosas por el estilo.  
				*[#lightgreen] Unidad de control \n  Se encarga precisamente junto con un reloj de establecer la cantidad de \n tiempo de procesamiento que lleva cada una de las aplicaciones 
		*[#GREEN] Almacenamiento
			*[#YELLOW] Almacenamiento a largo plazo \n Es decir cualquier información que se guarda por un periodo de tiempo largo y almacenamiento \n a corto plazo en el caso del almacenamiento a largo plazo el primer dispositivo del cuerpo \n debemos pensar hoy en día es el disco duro el disco duro tiene la capacidad de guardar \n información siempre y cuando se cuide ese disco duro que no se pegue algún tipo de imán o que \n no sufra un cambio específicamente en su estructura el disco duro tiene la capacidad de \n guardar información por cientos de años.	
			*[#YELLOW] Almacenamiento a corto plazo \n Nos referimos a el tipo de memoria como la memoria RAM la memoria RAM se encarga de la \n parte quizás más crucial qué es el que las aplicaciones se mantenga trabajando.
		*[#GREEN] Dispositivos entrada/salida
			*[#YELLOW] Dispositivos de entrada \n Los dispositivos de entrada son aquellos elementos que se conectan \n para introducir dentro del sistema general la función o información deseada. \n Algunos ejemplos de dispositivos de entrada son: \n*El teclado \n*El ratón \n*El escáner.
			*[#YELLOW] Dispositivos de salida \n Los dispositivos de salida son aquellos elementos que se conectan para que el sistema de \n procesamiento exponga la función o información deseada. \n Algunos ejemplos de dispositivos de salida son: \n*El monitor \n*Las memorias portátiles \n*Las impresoras.
		*[#GREEN] Buses
			*[#YELLOW] Un bus, es un canal de comunicación que las computadoras usan para comunicar sus componentes entre si. \n por ejemplo para comunicar el procesador con los periféricos, memoria o dispositivos de almacenamiento.
				*[#lightgreen] Los buses de datos: \n Este es un bus bidireccional, pues los datos pueden fluir \n hacia ó desde la CPU. Pueden ser entradas ó salidas,\n según la operación que se esté realizando (lectura o escritura) 
				*[#lightgreen] Los buses de control: \n Este conjunto de señales se usa para sincronizar las actividades\n y transacciones con los periféricos del sistema. \n Algunas de estas señales, como R / W , son señales que la CPU \n envía para indicar que tipo de operación se espera en ese momento. \n Los periféricos también pueden remitir señales de control a la CPU.	
				*[#lightgreen] Los buses de direcciones: \n Este es un bus unidireccional debido a que la información fluye \n es una sola dirección, de la CPU a la memoria \n ó a los elementos de entrada y salida. La CPU sola puede colocar \n niveles lógicos en las n líneas de dirección, con la cual se genera \n 2 posibles direcciones diferentes. Cada una de estas direcciones \n corresponde a una localidad de la memoria ó dispositivo de E / S.
	*[#ORANGE] Modelos
		*[#GREEN] El Modelo Harvard y el modelo de Neumann fueron pieza \n clave para el desarrollo de la computación es el diseño\n conceptual  y la estructura operacional fundamental de un \n sistema, Por tanto, un modelo es la descripción funcional de la \n implementación de varias partes que componen una computadora, los modelos \n de arquitectura clásica el modelo Harvard y el modelo de Von Neumann \n fueron modelos que dieron origen a la computación, aunque no son \n los únicos que existen hoy en día. 
			*[#YELLOW] Modelo Harvard
				*[#lightgreen] Tuvo el origen en el año de 1944 y toma su nombre de la \n computadora Harvard Mark creada por el científico Howard \n Aiken utilizaba dos memorias conectadas mediante buses \n   independientes a la unidad de procesamiento central una de \n la memoria contenía las instrucciones mientras que otra contenía la colección \n de datos los buses de ambas memorias son totalmente independientes esto \n permite que el CPU pueda acccesar de forma independiente y simultánea \n a la memoria de datos y a la de instrucciones Cómo los buses son \n independientes pueden contener diferente información en la \n misma ubicación de las diferentes memorias
			*[#Yellow] Modelo Neumann
				*[#lightgreen] Tuvo su origen en el año de 1945 cuando John Von Neumann junto \n con Mauchly y Eckert  modificaron la ENIAC para que funcionara \n con una sola unidad de almacenamiento en la cual se \n contenían todos los datos y las instrucciones de un programa. \n Este modelo es compuesto por un procesador la memoria central y las unidades \n de entrada y salida las cuales están interconectadas por los buses en \n la memoria central se encuentra almacenada a la información tanto del \n programa como el conjunto de datos hay un único espacio de memoria \n de lectura y escritura que contiene las instrucciones de \n los datos necesarios, el contenido de la memoria es accesible\n  por posición independientemente de que se acceda a datos o instrucciones la \n ejecución de las instrucciones se produce de manera secuencial.
@endmindmap
```
# Mapa 2
```plantuml
@startmindmap
*[#Pink] SuperComputadoras de México
	*[#Orange] La misión numero uno de una supercomputadora es de alguna forma sustituir\n lo que son los laboratorios tradicionales de investigación científica.
		*[#GREEN] Las primeras computadoras se fabricaron con bulbos por lo que requerían \n mucho espacio y  energía para su funcionamiento.
			*[#YELLOW] Gracias a la invención del transistor las computadoras se \n hicieron más chicas y fáciles de operar después con las tecnologías\n de circuitos integrados fue posible colocar más transistores en un \n procesador por lo que las computadoras se hicieron más rápidas cada vez.  
				*[#lightgreen] A mediados de los ochenta las computadoras dejaron de ser extraños /n artefactos exclusivos para científicos y superhéroes \n comenzando a familiarizarse su uso en oficinas y hogares 
	*[#Orange] Hace 61 años comenzó en México La era de la computación actualmente estamos \n tan habituados a está increíble herramienta que parece que siempre \n ha estado con nosotros y es que ahora resulta tan común que la encontramos \n prácticamente en cualquier lugar, pero no crean que siempre fue así.
		*[#GREEN] UNAM 
			*[#YELLOW] LA UNAM tuvo la primera gran computadora en Latinoamérica eso fue en 1958 y se \n trataba de una y IBM 650 que tenía una memoria de 2 K leía \nla información a través de tarjetas perforadas y  funcionaba con bulbos como los \n radios y las televisiones antiguas 
				*[#lightgreen] Era capaz de procesar 1000 operaciones aritméticas por segundo siendo \n su principal uso en la investigación científica
			*[#YELLOW] En 1991 la UNAM se consolida en las grandes ligas de la computación al adquirir la Cray  432 \n la primera supercomputadora de América Latina
				*[#lightgreen] La capacidad de esté súper cerebro electrónico equivalía a 2000 computadoras de\n oficina la Cray estuvo al servicio de la ciencia \n mexicana durante 10 años hasta Principios del nuevo siglo.  
			*[#YELLOW] Kan Balam
				*[#lightgreen] Esta es una supercomputadora con una capacidad equivalente a más de\n 1300 computadoras de escritorio con esta podemos realizar en tan solo\n  una semana cálculos que de otra forma nos  tomarían más de 25 años.\n En la universidad se utiliza para realizar proyectos de investigación en \n áreas como: \n*Astronomía  \n*Química cuántica  \n*Ciencia de materiales \n*Etc… \n Puede realizar 7 billones de operaciones Matemáticas por segundo 
			*[#YELLOW] Miztli
				*[#lightgreen] La supercomputadora de la UNAM amplio su capacidad a 8344 procesadores\n con lo que se resolverán problemas relacionados a la ciencia e \n investigación.Ahora con la ampliación Miztli cuenta con 58 servidores \n que aportan 1800 servidores
					*[#lightYellow] Actualmente tiene una velocidad de aproximadamente 228 teraflops lo cual\n significa 228 billones esto es  millones de millones de \n operaciones de punto flotante en un solo segundo.   
						*[#lightblue] Se ocupa en la prediccion del clima, estudiar los efectos de\n  los sismos, diseñar nuevos materiales asi como \n formulacion de farmacos.
			*[#YELLOW] BUAP
				*[#lightgreen] Hay un laboratorio que cuenta con una de las 5 supercomputadoras más\n  poderosas de América Latina tiene una capacidad de almacenamiento\n equivalente a casi 5000 computadoras portátiles y en tan solo un segundo \n puede ejecutar hasta dos mil millones de operaciones.
					*[#lightYellow] Uno de sus principales usos es el sector Automotriz \n y la aeronautica

@endmindmap